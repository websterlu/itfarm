package com.tc.itfarm.service;

import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.model.MenuArctile;

import java.util.List;

/**
 * Created by Administrator on 2016/9/5.
 */
public interface MenuArticleService extends BaseService<MenuArctile> {

    List<Integer> selectArticleByMenuId(Integer menuId, Page page);

    /**
     * 删除文章级联的记录
     * @param articleId
     */
    void deleteByArticleId(Integer articleId);

    /**
     * 批量更新
     * @param menuId （可为空，为空直接插入）
     * @param ids
     */
    void updateArticleIds(Integer menuId, Integer [] ids);
}
