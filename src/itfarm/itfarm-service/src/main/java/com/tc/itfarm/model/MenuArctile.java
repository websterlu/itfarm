package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class MenuArctile implements Serializable {
    private Integer recordId;

    private Integer menuId;

    private Integer articleId;

    private String articleTitle;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public MenuArctile(Integer recordId, Integer menuId, Integer articleId, String articleTitle, Date createTime) {
        this.recordId = recordId;
        this.menuId = menuId;
        this.articleId = articleId;
        this.articleTitle = articleTitle;
        this.createTime = createTime;
    }

    public MenuArctile() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle == null ? null : articleTitle.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}