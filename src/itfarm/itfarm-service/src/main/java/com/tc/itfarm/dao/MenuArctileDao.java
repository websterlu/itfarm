package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.MenuArctile;
import com.tc.itfarm.model.MenuArctileCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MenuArctileDao extends SingleTableDao<MenuArctile, MenuArctileCriteria> {
}