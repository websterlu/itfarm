package com.tc.itfarm.api.model;

import org.springframework.stereotype.Repository;

/**
 * 标识MyBatis的DAO,方便{@link org.mybatis.spring.mapper.MapperScannerConfigurer}的扫描。
 * 使用说明：所有DAO 定义成接口并继承MyBatisRepository，由Spring Context实现
 *
 */
@Repository
public interface MyBatisRepository {

}
