package com.tc.itfarm.api.common;

import java.io.Serializable;

/**
 * Created by wangdongdong on 2016/8/15.
 */
public class JsonMessage implements Serializable {

    public static final Integer STATUS_SUCCESS = 0;
    public static final Integer STATUS_FAIL = 1;
    public static final String STATUS_SUCCESS_MSG = "操作成功";
    /**
     * 返回结果代码 0成功 1失败
     */
    Integer resultCode;

    /**
     * 返回信息
     */
    String resultMessage;

    /**
     * 返回对象
     */
    Object result;

    boolean success;

    public JsonMessage() {
    }

    public JsonMessage(Integer resultCode, String resultMessage) {
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
        success = resultCode == STATUS_FAIL ? false : true;
    }

    public JsonMessage(Integer resultCode, String resultMessage, Object result) {
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
        this.result = result;
    }

    public static Integer getStatusSuccess() {
        return STATUS_SUCCESS;
    }

    public static Integer getStatusFail() {
        return STATUS_FAIL;
    }

    public static String getStatusSuccessMsg() {
        return STATUS_SUCCESS_MSG;
    }

    public Integer getResultCode() {
        return resultCode;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static JsonMessage toResult(Integer result, String successMsg, String failureMsg){
        JsonMessage jm = new JsonMessage();
        if (result > 0) {
            jm.setResultCode(JsonMessage.STATUS_SUCCESS);
            jm.setResultMessage(successMsg);
            jm.setSuccess(true);
        } else {
            jm.setResultCode(JsonMessage.STATUS_FAIL);
            jm.setResultMessage(failureMsg);
        }
        return jm;
    }
}
