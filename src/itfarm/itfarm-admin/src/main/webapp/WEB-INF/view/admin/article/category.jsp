<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div id="dcWrap">
 <jsp:include page="../header.jsp"></jsp:include>
 <div id="dcMain">
   <!-- 当前位置 -->
<div id="urHere">ITFARM 管理中心<b>></b><strong>文章分类</strong> </div>   <div class="mainBox" style="height:auto!important;height:550px;min-height:550px;">
        <h3><a href="${ctx }/category/addCategoryUI.do" class="actionBtn add">添加分类</a>文章分类</h3>
    <table width="100%" border="0" cellpadding="8" cellspacing="0" class="tableBasic">
     <tr>
        <th width="120" align="left">分类名称</th>
        <th align="left">简单描述</th>
        <th width="80" align="center">操作</th>
      </tr>
       <c:forEach items="${categorys }" var="item">
       <tr> 
        <td align="left"> <a href="">${item.name }</a></td>
        <td><c:if test="${item.description == null}">暂未添加</c:if>${item.description}</td>
        <td align="center"><a href="${ctx }/category/editCategoryUI.do?recordId=${item.recordId}">编辑</a> | <a href="${ctx }/category/deleteCategory.do?recordId=${item.recordId}">删除</a></td>
      </tr>
      </c:forEach>
          </table>
          <itfarm:PageBar pageUrl="${ctx }/category/list.do" pageAttrKey="page"></itfarm:PageBar>
           </div>
 </div>
 <div class="clear"></div>
<jsp:include page="../footer.jsp"></jsp:include>
</body>
</html>