<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <LINK href="${ctx}/css/index-min.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        .tb-toolbar ul li {
            margin-left: 0;
        }
    </style>
</head>
<body>
<div class="tb-toolbar">
    <div class="tb-toolbar-space" style="height: 3%;"></div>
    <ul class="tb-toolbar-list tb-toolbar-list-with-worthbuying tb-toolbar-list-with-cart tb-toolbar-list-with-history tb-toolbar-list-with-coupon">
        <li class="tb-toolbar-item-split"></li>

        <li class="tb-toolbar-item tb-toolbar-item-worthbuying" data-item="worthbuying"> <a href="#" class="tb-toolbar-item-hd">
            <div class="tb-toolbar-item-icon">
                
            </div>
            <div class="tb-toolbar-item-tipa">
                <iframe style="border:solid 1px #7ec8ea" name="weather_inc" src="http://i.tianqi.com/index.php?c=code&id=111" width="977" height="100" frameborder="no" marginwidth="0" marginheight="0" scrolling="no"></iframe>
                <div class="tb-toolbar-item-arrowa">
                    ◆
                </div>
            </div> </a>
            <div class="tb-toolbar-item-bd"></div>
        </li>
        <li class="tb-toolbar-item-split"></li>
        <li class="tb-toolbar-item tb-toolbar-item-cart" data-item="cart"> <a href="#" class="tb-toolbar-item-hd tb-toolbar-item-hd-cart J_TBToolbarCart">
            <div class="tb-toolbar-item-icon tb-toolbar-item-icon-cart">
                
            </div>
            <div class="tb-toolbar-item-label tb-toolbar-item-label-cart">
                收藏夹
            </div>
            <div class="J_ToolbarCartNum tb-toolbar-item-badge-cart">
                0
            </div>
            <div class="tb-toolbar-item-tip">
                我的收藏夹
                <div class="tb-toolbar-item-arrow">
                    ◆
                </div>
            </div> </a>
            <div class="tb-toolbar-item-bd tb-toolbar-mini-cart tb-toolbar-loading">
            </div>
        </li>
        <li class="tb-toolbar-item-split"></li>
        <li class="tb-toolbar-item tb-toolbar-history" data-item="history"> <a href="#" class="tb-toolbar-item-hd">
            <div class="tb-toolbar-item-icon">
                
            </div>
            <div class="tb-toolbar-item-tip">
                我的足迹
                <div class="tb-toolbar-item-arrow">
                    ◆
                </div>
            </div> </a>
            <div class="tb-toolbar-item-bd tb-toolbar-loading">
            </div>
        </li>
        <li class="tb-toolbar-item-split"></li>
        <%--<li class="tb-toolbar-item tb-toolbar-item-coupon" data-item="coupon"> <a href="#" class="tb-toolbar-item-hd tb-toolbar-item-hd-coupon">
            <div class="tb-toolbar-item-icon">
                
            </div>
            <div class="tb-toolbar-item-tip tb-toolbar-item-tip-coupon">
                店铺优惠
                <div class="tb-toolbar-item-arrow">
                    ◆
                </div>
            </div> </a>
            <div class="tb-toolbar-item-hd-extra">
                <div class="tb-toolbar-item-bubble tb-toolbar-item-bubble-coupon J_TBToolbarBubbleCoupon">
                    <span class="tb-toolbar-item-bubble-txt">该店铺可领优惠券</span>
                    <br />
                    <a href="#" class="tb-toolbar-item-bubble-btn J_TBToolbarBubbleOpenCouponTgr">立即领取</a>
                    <span class="tb-toolbar-item-arrow">◆</span>
                    <span class="tb-toolbar-item-bubble-saw"></span>
                </div>
            </div>
            <div class="tb-toolbar-item-bd tb-toolbar-item-bd-coupon tb-toolbar-loading"></div>
        </li>--%>
    </ul>
    <div class="tb-toolbar-space" style="height: 7%;"></div>
    <ul class="tb-toolbar-list tb-toolbar-list-with-feedback tb-toolbar-list-with-gotop">
        <li class="tb-toolbar-item" data-item="feedback"> <a href="#" class="tb-toolbar-item-hd">
            <div class="tb-toolbar-item-icon">
                
            </div>
            <div class="tb-toolbar-item-tip">
                <span class="tb-toolbar-item-tip-text">反馈</span>
                <div class="tb-toolbar-item-arrow">
                    ◆
                </div>
            </div> </a>
        </li>
        <li class="tb-toolbar-item" data-item="gotop"> <a href="#" class="tb-toolbar-item-hd">
            <div class="tb-toolbar-item-icon">
                
            </div>
            <div class="tb-toolbar-item-tip">
                <span class="tb-toolbar-item-tip-text">顶部</span>
                <div class="tb-toolbar-item-arrow">
                    ◆
                </div>
            </div> </a>
        </li>
    </ul>
</div>
</body>
</html>